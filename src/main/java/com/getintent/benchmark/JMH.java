package com.getintent.benchmark;

import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

import static org.openjdk.jmh.runner.options.TimeValue.seconds;

public class JMH {

    public static void main(String[] argv) throws Exception {
        Options options = new OptionsBuilder()
                .exclude(".?BenchmarkUnsafe.?")
                .mode(Mode.AverageTime)
                .forks(1)
                .timeout(seconds(10))
                .threads(4)
                .timeUnit(TimeUnit.MILLISECONDS)
                .measurementIterations(5)
                .measurementBatchSize(1000)
                .measurementTime(seconds(10))
                .warmupIterations(3)
                .warmupBatchSize(1000)
                .warmupTime(seconds(5))
                .build();

        new Runner(options).run();
    }
}
