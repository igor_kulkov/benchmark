package com.getintent.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import sun.misc.Unsafe;

import java.lang.reflect.Field;

@State(Scope.Thread)
public class BenchmarkUnsafe {

    private static final Unsafe UNSAFE;
    private static final long OFFSET;

    private long GAMMA = 0x9e3779b97f4a7c15L;
    private int count = 1000;
    private long value;

    static {
        try {
            Field unsafe = Unsafe.class.getDeclaredField("theUnsafe");
            unsafe.setAccessible(true);
            UNSAFE = (Unsafe) unsafe.get(null);
            OFFSET = UNSAFE.objectFieldOffset(BenchmarkUnsafe.class.getDeclaredField("value"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Benchmark
    public long direct(BenchmarkUnsafe b) {
        for (int i = 0; i < b.count; i++) {
            b.value += b.GAMMA;
        }

        return b.value;
    }

    @Benchmark
    public long unsafe(BenchmarkUnsafe b) {
        long r = 0;
        for (int i = 0; i < b.count; i++) {
            UNSAFE.putLong(b, OFFSET, r = UNSAFE.getLong(b, OFFSET) + b.GAMMA);
        }

        return r;
    }
}