package com.getintent.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@State(Scope.Thread)
public class BenchmarkRandom {

    private int count = 1000;
    private Random builtin, custom;

    @Setup
    public void setup() {
        builtin = ThreadLocalRandom.current();
        custom = FastThreadLocalRandom.current();
    }

    @Benchmark
    public double builtin(BenchmarkRandom b) {
        double sum = 0.;
        for (int i = 0; i < b.count; i++) {
            sum += b.builtin.nextGaussian();
        }

        return sum;
//
//        return b.builtin.nextGaussian();
    }

    @Benchmark
    public double custom(BenchmarkRandom b) {
        double sum = 0.;
        for (int i = 0; i < b.count; i++) {
            sum += b.custom.nextGaussian();
        }

        return sum;

//        return b.custom.nextGaussian();
    }
}
